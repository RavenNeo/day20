(function() {
    angular
        .module("Shop")         
        .controller("SearchCon", SearchCon);    
   
      SearchCon.$inject = ["$window","SAService","$state"];

    function SearchCon($window,SAService,$state) {
        var searchCon = this;

        // Exposed data models
        // This will allow us apply two-way data-binding to this object by using ng-model in our view (i.e., index.html)
        searchCon.searchString = "";

        // Creates a status object. We will use this to display appropriate success or error messages.
        searchCon.status = {
            message: "",
            code: ""
        };

        searchCon.search = search;
        searchCon.products = [];
        searchCon.selectOne = selectOne;
      
        function search() {
            //Calls the search service retrieveA function
            SAService
                .retrieveA(searchCon.searchString)
                .then(function(results){
                    console.log(JSON.stringify(results.data));
                    searchCon.products= results.data;
                })
                .catch(function(err){
                    console.log("error " + err);
                });

        } // END function search()

        function selectOne(id){
            $state.go("editProductForm",{id:id});
        }
    } // END SearchCon
})();
