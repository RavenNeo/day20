// Always use an IIFE, i.e., (function() {})();
(function () {
    // Attaches DeptService service to the DMS module
    angular
        .module("Shop")
        .service("SAService", SAService);

    // Dependency injection. Here we inject $http because we need this built-in service to communicate with the server
    // There are different ways to inject dependencies; $inject is minification safe
    SAService.$inject = ['$http'];

    // DeptService function declaration
    // Accepts the injected dependency as a parameter. We name it $http for consistency, but you may assign any name
    function SAService($http) {

        // Declares the var service and assigns it the object this (in this case, the DeptService). Any function or
        // variable that you attach to service will be exposed to callers of DeptService, e.g., search.controller.js
        // and register.controller.js
        var service = this;

        // EXPOSED FUNCTIONS -------------------------------------------------------------------------------------------
        // TODO: 3.2 Expose retrieveDepartments function
        service.retrieveA = retrieveA;
        service.retrieveO = retrieveO;
        // FUNCTION DECLARATION AND DEFINITION -------------------------------------------------------------------------
        // TODO: 3.1 Create function that would retrieve departments
        
        function retrieveA(searchString) {
        
            return $http({
                method: 'GET'
                , url: 'api/products'
                , params:{
                        searchString:searchString
                }
            });
        }

        function retrieveO(id) {
        
            return $http({
                method: 'GET'
                , url: 'api/products/'+id
            });
        }
    }
})();