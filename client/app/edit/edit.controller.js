(function() {
    angular
        .module("Shop")         
        .controller("EditCon", EditCon);    
   
      EditCon.$inject = ["$window","$state","SAService","$stateParams"];

    function EditCon($window,$state,SAService,$stateParams) {
        var editCon = this;

        editCon.id ="";

        editCon.result = {};
        // Creates a status object. We will use this to display appropriate success or error messages.
        editCon.status = {
            message: "",
            code: ""
        };

        editCon.searchOne = searchOne;
        editCon.initDetails = initDetails;


        //initDetails();

        if($stateParams.id){
            editCon.id = $stateParams.id;
            editCon.searchOne();
        }

        //searchCon.search = search;
        //searchCon.selectOne = selectOne;

         function initDetails() {
            console.log("-- show.controller.js > initDetails()");
            editCon.result.id = null;
            editCon.result.upc12 = null;
            editCon.result.brand = "";
            editCon.result.name = "";
        }

        function searchOne() {
            //Calls the search service retrieveA function
            initDetails();
            SAService
                .retrieveO(editCon.id)
                .then(function(result){
                    console.log(JSON.stringify(result.data));
                    editCon.result.id = result.data.id;
                    editCon.result.upc12 = result.data.upc12;
                    editCon.result.brand = result.data.brand;
                    editCon.result.name = result.data.name;
                })
                .catch(function(err){
                    console.log("error " + err);
                });
        } // END function search()
/*
        function initDetails() {
            console.log("-- show.controller.js > initDetails()");
            editCon.id = "",
            editCon.upc12 = "",
            editCon.brand = "",
            editCon.name = ""

        }
        */
    } // END SearchCon
})();
