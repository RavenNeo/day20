//IIEF--Immediately-invoked function expression
(function(){
    angular
        .module("Shop")
        .config(clientRoutes);
        
    clientRoutes.$inject = ["$stateProvider", "$urlRouterProvider"];

    function clientRoutes($stateProvider, $urlRouterProvider){
        $stateProvider
            .state("search", {
                url: "/search",
                templateUrl: "./app/search/search.html",
                controller: "SearchCon as con",
            })
            .state("editProductForm", {
                url: "/edit/:id",
                templateUrl: "./app/edit/edit.html",
                controller: "EditCon as econ",
            });

        $urlRouterProvider.otherwise("/search");
    }
})();
