// Handles API routes
module.exports = function(app, db) {
  var Products = require("./api/product.controller")(db);

  // Retrieve product using post
  app.get("/api/products", Products.retrieveProducts);

  app.get("/api/products/:id", Products.retrieveOne);
  //app.post("/api/products",Products)

};