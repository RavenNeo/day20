var retrieveProducts = function(db){
    console.log('\nInformation from server:')
    return function (req, res) {
        console.log('\nRetrieve Data');
        var where = {}
        if(req.query.searchString){
            where.searchString = req.query.searchString;
        }
        db.Products
            .findAll({
                where:{

                    $or: [
                        {name: {like: "%" +  where.searchString + "%"}},
                        {brand : {like: "%" +  where.searchString + "%"}},
                    ]
                }
                , limit: 20
                , order: [
                    ['name','ASC']
                ]
            })
            .then(function(Products){
                res.status(200);
                res.json(Products)
            })
            .catch(function(err) {
        console.log("ProductsDB error clause: " + err);
        res
          .status(500)
          .json(err);
      });
    };
};

var retrieveOne = function(db){
    console.log('\nInformation from server:')
    return function (req, res) {
        console.log("hihi");
        db.Products
            .findOne({
                where:{id:req.params.id}
            })
            .then(function(Products){
                res.status(200);
                res.json(Products)
            })
            .catch(function(err) {
        console.log("ProductsDB error clause: " + err);
        res
          .status(500)
          .json(err);
      });
    };
};

/*
var editProducts = function(db){
    console.log("Editing");
    return function(req, res){
        db.Products
            .update(
                {upc12:req.upc12}
                )
    };
};
*/
// Export route handlers
module.exports = function(db) {
  return {
    retrieveProducts: retrieveProducts(db),
    retrieveOne: retrieveOne(db),
    //editProducts: editProducts(db)
  }
};
